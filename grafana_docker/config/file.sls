# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import grafana_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = grafana_docker.docker.mountpoint %}

{%- for dir in [
  'docker/grafana/data',
  'docker/influxdb/db',
  ]
%}
grafana-docker-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: "0755"
    - file_mode: "0644"
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

{#
grafana-docker-config-file-file-managed:
  file.managed:
    - name: {{ grafana_docker.config }}
    - source: {{ files_switch(['example.tmpl'],
                              lookup='grafana-docker-config-file-file-managed'
                 )
              }}
    - mode: 644
    - user: root
    - group: {{ grafana_docker.rootgroup }}
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        grafana_docker: {{ grafana_docker | json }}
        #}
