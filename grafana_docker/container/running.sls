# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import grafana_docker with context %}

{%- set mountpoint = grafana_docker.docker.mountpoint %}

grafana-docker-container-running-container-present:
  docker_image.present:
    - name: {{ grafana_docker.container.image }}
    - tag: {{ grafana_docker.container.tag }}
    - force: True

grafana-docker-container-running-container-managed:
  docker_container.running:
    - name: {{ grafana_docker.container.name }}
    - image: {{ grafana_docker.container.image }}:{{ grafana_docker.container.tag }}
    - restart: always
    - user: 1000
    - binds:
      - {{ mountpoint }}/docker/grafana/data:/var/lib/grafana
    - port_bindings:
      - 3000:3000

grafana-docker-container-running-influx-db-container-present:
  docker_image.present:
    - name: {{ grafana_docker.influxdb.container.image }}
    - tag: {{ grafana_docker.influxdb.container.tag }}
    - force: True

grafana-docker-container-running-influx-db-container-managed:
  docker_container.running:
    - name: {{ grafana_docker.influxdb.container.name }}
    - image: {{ grafana_docker.influxdb.container.image }}:{{ grafana_docker.influxdb.container.tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/influxdb/db:/var/lib/influxdb
    - port_bindings:
      - 8086:8086
{#
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=false"
      - "traefik.http.middlewares.grafana-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.grafana-web.middlewares=grafana-redirect-websecure"
      - "traefik.http.routers.grafana-web.rule=Host(`{{ grafana_docker.container.url }}`)"
      - "traefik.http.routers.grafana-web.entrypoints=web"
      - "traefik.http.routers.grafana-websecure.rule=Host(`{{ grafana_docker.container.url }}`)"
      - "traefik.http.routers.grafana-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.grafana-websecure.tls=true"
      - "traefik.http.routers.grafana-websecure.entrypoints=websecure"
#}
